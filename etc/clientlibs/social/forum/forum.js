/*
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */

(function (CQ, $CQ) {
    CQ.soco = CQ.soco || {};
    CQ.soco.forum = CQ.soco.forum || {};

    CQ.soco.forum.onReplyTopic = function (postID, postMessage) {
        var mainTextArea = $CQ("#" + encodedTopicId + "-text");
        var quoteText = "[quote:postID=" + postID + "]" + postMessage + "[/quote]";
        addQuoteToArea(mainTextArea, quoteText);
    }

    CQ.soco.forum.onReplyPost = function (btn, postID, postMessage) {
        var mainTextArea = $CQ("#" + encodedTopicId + "-text");
        try {
            var replyFormCss = CQ.soco.forum.getClosestElement(btn, ".reply-form").css('display');
            var quoteText = "[quote:postID=" + postID + "]" + postMessage + "[/quote]";
            if (replyFormCss === 'block') {
                var textArea = CQ.soco.forum.getClosestElement(btn, "textarea.comment-text");
                addQuoteToArea(textArea, quoteText);
            } else {
                addQuoteToArea(mainTextArea, quoteText);
            }
        } catch (exception_var) {
            mainTextArea.val(exception_var);
        }
    }

    CQ.soco.forum.getClosestElement = function (base, element) {
        if (base == null) return null;
        if ($CQ(base).find(element).length > 0) return $CQ(base).find(element);
        return CQ.soco.forum.getClosestElement(base.parentNode, element);
    }

    function addQuoteToArea(textArea, text) {
        if (escape(textArea.val()).replace(/([^>\%0D\%0A]?)(\%0D\%0A|%0A\%0D|\%0D|%0A)/g, '').indexOf(escape(text).replace(/([^>\%0D\%0A]?)(\%0D\%0A|%0A\%0D|\%0D|%0A)/g, '')) == -1) {
            textArea.val(textArea.val() + text);
        }
        $CQ('body,html').animate({scrollTop: textArea.offset().top});
    }
})(CQ, $CQ);
module.exports = function(grunt) {

  grunt.initConfig({
	prettify: {
		options: {
			  "wrap_line_length":0
		},
		all: {
			expand: true,
			cwd:'content/',
			ext:".html",
			ext: '.html',
			src: ['**/*.html','!geometrixx-media/en/community.topic.html'],
			dest: 'content/'
		}
	}
  });

/*	grunt.loadNpmTasks('js-beautify');*/
	grunt.loadNpmTasks('grunt-prettify');


};
/*************************************************************************
*
* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2012 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
**************************************************************************/

/**
 * @class CQ.ipe.ArticleEditor
 * @extends CQ.ipe.PlainTextEditor
 * @private
 * This class implements the inplace editing of articles. It is registered as editor type
 * "article" and may be used for title components.
 * @since 5.3
 * @constructor
 * Create a new ArticleEditor.
 * @param {Object} config The configuration of the editor
 */
CQ.ipe.ArticleEditor = CQ.Ext.extend(CQ.ipe.PlainTextEditor, {

    /**
     * @cfg {String} textPropertyName
     * Name of the property where the path to the article asset is stored in this component; defaults to
     * "./fileReference".
     */

    renditionPath:null,
    
    fileReference:null,

    constructor: function(config) {
        config = config || { };
        CQ.Util.applyDefaults(config, {
            "textPropertyName": "./fileReference",
            "enterKeyMode": "ignore"
        });
        CQ.ipe.ArticleEditor.superclass.constructor.call(this, config);
    },

    
    start: function(editComponent, compElement, initialContent) {
        this.fileReference = initialContent;
        this.renditionPath = CQ.ipe.ArticleEditor.knownPaths[this.fileReference];
        
        CQ.HTTP.get(this.renditionPath, function(options, success, response) {
            if (success) {
                initialContent = response.responseText;
                CQ.ipe.ArticleEditor.superclass.start.call(this, editComponent, compElement, initialContent);
                document.execCommand("insertBrOnReturn",false,true);
                this.textContainer.innerHTML = this.textContainer.innerHTML.replace(/\n/g,"<br>")
            } else {
                throw "Could not load article";
            }
        }, this);
    },
    
    finish: function() {
        var editedContent =  this.getHtmlContent().replace(/<br>/g,"\n").replace(/<div><br><\/div>/g,"").replace(/<div>/g,"\n").replace(/<\/div>/g,"");
        var params = { "_charset_":"utf-8" };
        params["./jcr:lastModified"] = "";
        params["./jcr:lastModifiedBy"] = "";
        params["./jcr:data"] = editedContent;
        
        var component = this.editComponent;
        var self = this;
        CQ.HTTP.post(this.renditionPath+"/jcr:content",
        function(options, success, response){
            if(success){
               component.fireEvent(CQ.wcm.EditBase.EVENT_AFTER_EDIT, component);
               self.addUndoStep(editedContent);
            }else{
                throw "Could not save changes to article";
            }
        },params);
        
    },
    
    handleKeyDown: function(e) {
        if (!window.CQ_inplaceEditDialog) {
            if (e.getCharCode() === e.ENTER) {
                
            }
        }
    }

});


CQ.ipe.InplaceEditing.register("article", CQ.ipe.ArticleEditor);

CQ.ipe.ArticleEditor.knownPaths = {};
/*************************************************************************
*
* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2012 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
**************************************************************************/

(function(){

var unitPattern = /^([0-9]+(\.[0-9]+)?(px|em|ex|%|in|cm|mm|pt|pc))$/; 

/*
 * VType to validate field is a valid CSS Unit
 *
*/
CQ.Ext.form.VTypes.cssunit = function(value, field){
    return value.length==0 || unitPattern.test(value);
}
    
CQ.Ext.form.VTypes.cssunitText = CQ.I18n.getMessage("Must be a valid CSS length. ex: 10px or 2.3em");
    
})();


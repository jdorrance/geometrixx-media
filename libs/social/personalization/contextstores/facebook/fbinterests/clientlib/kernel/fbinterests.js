/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

if (!CQ_Analytics.FacebookInterestsMgr) {
    CQ_Analytics.FacebookInterestsMgr = function() {};

    CQ_Analytics.FacebookInterestsMgr.prototype = new CQ_Analytics.SessionStore();

    /**
     * @cfg {String} STOREKEY
     * Store internal key
     * @final
     * @private
     */

    CQ_Analytics.FacebookInterestsMgr.prototype.STOREKEY = "FBINTERESTSDATA";

    /**
     * @cfg {String} STORENAME
     * Store internal name
     * @final
     * @private
     */

    CQ_Analytics.FacebookInterestsMgr.prototype.STORENAME = "fbinterests";

    /**
     * {@inheritDoc}
     */
    CQ_Analytics.FacebookInterestsMgr.prototype.getLabel = function(name) {
        return name;
    };

    /**
     * {@inheritDoc}
     */
    CQ_Analytics.FacebookInterestsMgr.prototype.getLink = function(name) {
        return "";
    };

    /**
     * {@inheritDoc}
     */
    CQ_Analytics.FacebookInterestsMgr.prototype.clear = function() {
        this.data = null;
        this.initProperty = {};

    };

    CQ_Analytics.FacebookInterestsMgr.prototype.getLoaderURL = function() {
        return CQ_Analytics.ClientContextMgr.getClientContextURL("/contextstores/fbinterestsdata/loader.json");
    };

    CQ_Analytics.FacebookInterestsMgr.prototype.loadProfile = function(authorizableId) {
        if(authorizableId) {
            var url = this.getLoaderURL();
            url = CQ_Analytics.Utils.addParameter(url, "authorizableId", authorizableId);
            CQ_Analytics.FacebookInterestsMgr.lastUid = authorizableId;
            try {

                 // the response body will be empty if the authorizableId doesn't resolve to a profile
                    var object = CQ.shared.HTTP.eval(url);
                    if (object) {
                        this.data = {};
                        for (var p in object) {
                            this.data[p] = object[p];
                        }
                        if (CQ_Analytics.ClickstreamcloudEditor) {
                            CQ_Analytics.ClickstreamcloudEditor.reload();
                        }
                        return true;
                    }


            } catch(error) {
                if (console && console.log) console.log("Error during profile loading", error);
            }
        }
        return false;
    };


    CQ_Analytics.FacebookInterestsMgr = new CQ_Analytics.FacebookInterestsMgr();

    CQ_Analytics.CCM.addListener("configloaded", function() {

        CQ_Analytics.ProfileDataMgr.addListener("update", function() {
            var uid = CQ_Analytics.ProfileDataMgr.getProperty("authorizableId");
            if (uid != this.lastUid) {
                CQ_Analytics.FacebookInterestsMgr.loadProfile(uid);
                this.fireEvent("update");
            }
        },CQ_Analytics.FacebookInterestsMgr);

        //registers FB Interests  Data to clickstreamcloud manager
        CQ_Analytics.CCM.register(this);

    }, CQ_Analytics.FacebookInterestsMgr);

}

/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/
/**
 * The <code>CQ_Analytics.FacebookProfileDataMgr</code> object is a store providing user's facebook profile information.
 */
if (!CQ_Analytics.FacebookProfileDataMgr) {

    CQ_Analytics.FacebookProfileDataMgr = function() {};

    CQ_Analytics.FacebookProfileDataMgr.prototype = new CQ_Analytics.SessionStore();

    /**
     * @cfg {String} STOREKEY
     * Store internal key
     * @final
     * @private
     */

    CQ_Analytics.FacebookProfileDataMgr.prototype.STOREKEY = "FBPROFILEDATA";

    /**
     * @cfg {String} STORENAME
     * Store internal name
     * @final
     * @private
     */

    CQ_Analytics.FacebookProfileDataMgr.prototype.STORENAME = "facebookprofile";

    /**
     * {@inheritDoc}
     */
    CQ_Analytics.FacebookProfileDataMgr.prototype.clear = function() {
        this.data = null;
        this.initProperty = {};
    };

    CQ_Analytics.FacebookProfileDataMgr.prototype.init = function() {
        if (!this.data) {
            this.data = {};
            for (var p in this.initProperty) {
                this.data[p] = this.initProperty[p];
            }
        }
    };

    CQ_Analytics.FacebookProfileDataMgr.prototype.getLoaderURL = function() {
        return CQ_Analytics.ClientContextMgr.getClientContextURL("/contextstores/fbprofiledata/loader.json");
    };

    CQ_Analytics.FacebookProfileDataMgr.prototype.loadProfile = function(authorizableId) {
        if(authorizableId) {
            CQ_Analytics.FacebookProfileDataMgr.lastUid = authorizableId;
            var url = this.getLoaderURL();
            url = CQ_Analytics.Utils.addParameter(url, "authorizableId", authorizableId);

            try {

                // the response body will be empty if the authorizableId doesn't resolve to a profile
                var object = CQ.shared.HTTP.eval(url);
                if (object) {
                    this.data = {};
                    for (var p in object) {
                        this.data[p] = object[p];
                    }
                    this.fireEvent("update");
                    this.fireEvent("checkBirthday");
                    if (CQ_Analytics.ClickstreamcloudEditor) {
                        CQ_Analytics.ClickstreamcloudEditor.reload();
                    }
                    return true;
                }


            } catch(error) {
                if (console && console.log) console.log("Error during profile loading", error);
            }
        }
        return false;
    };

    CQ_Analytics.FacebookProfileDataMgr = new CQ_Analytics.FacebookProfileDataMgr();

    CQ_Analytics.CCM.addListener("configloaded", function() {

        CQ_Analytics.ProfileDataMgr.addListener("update", function() {
            var uid = CQ_Analytics.ProfileDataMgr.getProperty("authorizableId");
            if (uid != this.lastUid) {
                CQ_Analytics.FacebookProfileDataMgr.loadProfile(uid);
                this.fireEvent("update");
            }
        },CQ_Analytics.FacebookProfileDataMgr);

        //registers Profile Data to clickstreamcloud manager
        CQ_Analytics.CCM.register(this);

    }, CQ_Analytics.FacebookProfileDataMgr);

}

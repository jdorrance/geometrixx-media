/*
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */

function CQ_collab_forum_searchbox_handleOnFocus(el, message) {
    if (el.value == message) {
        el.value = "";
        el.className = el.className + " active";
    }
}

function CQ_collab_forum_searchbox_handleOnBlur(el, message) {
    if (el.value == "") {
        el.value = message;
        el.className = el.className.replace(" active", "");
    }
}

function CQ_collab_forum_composer_validate_topic(form) {
    var valid = form["jcr:title"].value.trim().length > 0 && form["jcr:description"].value.trim().length > 0;

    if (!valid) {
        alert("Please enter a subject and message.");
        form["jcr:title"].focus();
    }

    return valid;
}

function CQ_collab_forum_composer_validate_post(form, message) {
    var valid = form["jcr:description"].value.trim().length > 0 && form["jcr:description"].value != message;

    if (!valid) {
        alert("Please enter a message.");
        form["jcr:description"].focus();
    }

    return valid;
}

/**
 * Replaces the subject field's value with an empty string if the value equals to the given message.
 * Furthermore it applies the "expanded" CSS class to the surrounding composer div. The reverse operation, if no input
 * was done, happens on blur (see #CQ_collab_forum_composer_handleOnBlur).
 *
 * @param {HTMLElement} el The form input field.
 * @param {String} message The message.
 * @see #CQ_collab_forum_composer_handleOnBlur_topic
 */
function CQ_collab_forum_composer_handleOnFocus_topic(el, message) {
    if (el.value == message) {
        el.value = "";
    }

    // set expanded style on composer div
    var composer = el.form.parentNode;
    if( composer.className.indexOf("expanded") == -1) {
        composer.className = composer.className + " expanded";
    }
}

/**
 * Replaces the subject field's value with the given message if it and the textarea are previously empty.
 * Furthermore in that case it removes the "expanded" CSS class from the surrounding composer div. This is the reverse
 * operation of #CQ_collab_forum_composer_handleOnFocus.
 *
 * @param {HTMLElement} el The form input field.
 * @param {String} message The message.
 * @see #CQ_collab_forum_composer_handleOnFocus_topic
 */
function CQ_collab_forum_composer_handleOnBlur_topic(el, message) {
    var textarea = el.form["jcr:description"];
    var composer = el.form.parentNode;
    if (el.value == "" && textarea.value == "") {
        el.value = message;

        // remove expanded style from composer div
        composer.className = composer.className.replace(" expanded", "");
    }
}

function CQ_collab_forum_composer_cancel(el) {
    var composer = el.form.parentNode;
    // remove expanded style from composer div
    composer.className = composer.className.replace(" expanded", "");
    el.form.reset();
}

function CQ_collab_forum_openCollabAdmin() {
    CQ.shared.Util.open(CQ.shared.HTTP.externalize('/socoadmin.html#/content/usergenerated' + CQ.WCM.getPagePath()));
}

(function(CQ, $CQ) {
    "use strict";
    CQ.soco = CQ.soco || {};
    CQ.soco.forum = CQ.soco.forum || {};

    /**
     * Replaces the text field's value with the given message if the textarea are previously empty.
     * Furthermore in that case it removes the "expanded" CSS class from the surrounding composer div. This is the reverse
     * operation of #CQ_collab_forum_composer_handleOnFocus.
     *
     * @param {HTMLElement} el The form input field.
     * @param {String} message The message.
     * @see #CQ_collab_forum_composer_handleOnFocus_topic
     */
    CQ.soco.forum.topicCreatorBlur = function (element, message) {
        if (element.value === "") {
            element.value = message;
            $CQ(element.form).toggleClass("expanded");
            $CQ(element.form).find(".topicDetails").toggle();
        }
    };

    /**
     * Replaces the text field's value with an empty string if the value equals to the given message.
     * The reverse operation, if no input was done, happens on blur (see #CQ_collab_forum_composer_handleOnBlur).
     *
     * @param {HTMLElement} el The form input field.
     * @param {String} message The message.
     * @see #CQ_collab_forum_composer_handleOnBlur_post
     */
    CQ.soco.forum.topicCreatorFocus = function (element, message) {
        if (element.value === message) {
            element.value = "";
            $CQ(element.form).toggleClass("expanded");
            $CQ(element.form).find(".topicDetails").toggle();
        }
    };
    CQ.soco.forum.attachToTopicComposer = function(targetForm, resourcePath) {
        var success = function(data, status, jqxhr) {
            window.location = jqxhr.getResponseHeader('Location');
        },
        failure = function(jqxhr, status) {
            throw status;
        };
        CQ.soco.commons.clientSideComposer(targetForm, "listitem-template", success, failure);
    };
})(CQ, $CQ);
